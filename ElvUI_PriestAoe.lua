﻿------------------------------------------------------------------------------
--	EPA by InKahootz
--    Special thanks to Slaren for GridStatusPriestAoe
------------------------------------------------------------------------------
local AddOnName = select(1,...)
local E, L, V, P, G, _ = unpack(ElvUI); --Import: Engine, Locales, PrivateDB, ProfileDB, GlobalDB, Localize Underscore
local EPA = E:NewModule('EPA', 'AceHook-3.0', 'AceEvent-3.0', 'AceConsole-3.0');
local UF = E:GetModule('UnitFrames');
local DTP
local EP = LibStub("LibElvUIPlugin-1.0")

EPA:UnregisterAllEvents()

EPA.Title = select(2, GetAddOnInfo(AddOnName))
EPA.Version = GetAddOnMetadata(AddOnName, 'Version')
EPA.TicketTracker = 'http://www.tukui.org/tickets/elvuipriestaoe/'
EPA.TexCoords = E.TexCoords
EPA.MyClass = E.myclass
EPA.MyName = E.myname
EPA.MyRealm = E.myrealm
EPA.MyGUID = E.myguid
EPA.Noop = function() return end

-- constants
local TALENTTREE_DISCIPLINE = 1
local TALENTTREE_HOLY = 2
local SPELLID_COH = 34861
local SPELLID_POH = 596
local SPELLID_CHAKRA_SANCT = 81206	-- Chakra: Sanctuary
local GLYPH_COH = 55675
local GLYPH_CH  = 41522
local ICON_COH = "Interface\\Icons\\Spell_Holy_CircleOfRenewal"
local ICON_POH = "Interface\\Icons\\Spell_Holy_PrayerOfHealing02"
local ICON_HN = "Interface\\Icons\\Spell_Holy_HolyNova"
local ICON_CH = "Interface\\Icons\\Spell_Nature_HealingWaveGreater"
local ICON_HR = "Interface\\Icons\\Spell_Paladin_DivineCircle"
local COH_BASE = (4599 + 5082) / 2
local POH_BASE = (8450 + 8927) / 2
local HN_BASE =  (2373 + 2756) / 2
local CH_BASE =  (7086 + 8094) / 2
local HR_Base =  (5098 + 6230) / 2
local SPELLMOD_COH = 0.467
local SPELLMOD_POH = 0.838
local SPELLMOD_HN  = 0.247
local SPELLMOD_CH  = 0.6876
local SPELLMOD_HR  = 0.675

local PARTY_STATE_GROUP_COUNT = {
	["solo"] = 1,
	["party"] = 1,
	["Raid40"] = 8,
	["Raid25"] = 5,
	["Raid10"] = 2,
	["bg"] = 8,
	["arena"] = 1
}

-- locals
local EPA = EPA
local UnitGUID = UnitGUID
local GetPlayerMapPosition = GetPlayerMapPosition
local SetMapToCurrentZone = SetMapToCurrentZone
local GetSpellCooldown = GetSpellCooldown
local UnitHealth = UnitHealth
local UnitHealthMax = UnitHealthMax
local UnitIsVisible = UnitIsVisible
local UnitIsDeadOrGhost = UnitIsDeadOrGhost
local UnitIsConnected = UnitIsConnected
local UnitIsEnemy = UnitIsEnemy
local UnitIsCharmed = UnitIsCharmed
local UnitIsUnit = UnitIsUnit
local UnitInRange = UnitInRange
local UnitBuff = UnitBuff
local UnitGetIncomingHeals = UnitGetIncomingHeals
local GetSpellCritChance = GetSpellCritChance
local GetSpellBonusHealing = GetSpellBonusHealing
local GetSpecialization = GetSpecialization
local math_min = math.min
local math_max = math.max
local math_floor = math.floor
local tinsert = table.insert
local tsort = table.sort
local select = select
local pairs = pairs
local wipe = wipe
local tostring = tostring
local WorldMapFrame = WorldMapFrame
local raid10visibility = SecureCmdOptionParse(E.db.unitframe.units.raid10.visibility)
local raid25visibility = SecureCmdOptionParse(E.db.unitframe.units.raid25.visibility)
local raid40visibility = SecureCmdOptionParse(E.db.unitframe.units.raid40.visibility)
local raid10groups = E.db.unitframe.units.raid10.numGroups
local raid25groups = E.db.unitframe.units.raid25.numGroups
local raid40groups = E.db.unitframe.units.raid40.numGroups

-- local data
local settings
local settings_coh
local settings_poh
local settings_ch
local settings_hn
local settings_hr
local settings_debug
local num_enabled = 0
local update_frame = CreateFrame("Frame")
local coh_prev_best = {
	uid = nil,
	guid = nil,
	amount = -1
}
local hr_prev_best = {
	uid = nil,
	guid = nil,
	amount = -1
}
local poh_prev_best
local spellname_gopoh
local coh_targets
local coh_coef_madd
local coh_coef_m
local poh_coef_madd
local poh_coef_m
local hn_coef_madd
local hn_coef_m
local ch_coef_madd
local ch_coef_m
local update_timer = 0
local ch_range
local num_groups = 1
local test_of_faith_mul
local roster
local player_group
local player_guid

local auraAnchors = {
	TOP = 'TOP',
	BOTTOM = 'BOTTOM',
	LEFT = 'LEFT',
	RIGHT = 'RIGHT',
	CENTER = 'CENTER',
	TOPLEFT = 'TOPLEFT',
	TOPRIGHT = 'TOPRIGHT',
	BOTTOMRIGHT = 'BOTTOMRIGHT',
	BOTTOMLEFT = 'BOTTOMLEFT',
};

P['epa'] = {};

P['epa'] = {
	['debug'] = {
		['one'] = false,
		['two'] = false,
		['three'] = false,
		['four'] = false,
		['testmode'] = false,
	},
	['cycle_time'] = 0.3,
	['first_groups_only'] = true,
	['targets_hit'] = true,
	['coh'] = {
		['enable'] = false,
		['color'] = { r=1, g=1, b=0, a=1},
		['threshold'] = 60000,
		['enable_if_have'] = true,
		['hidecd'] = true,
	},
	['poh'] = {
		['enable'] = true,
		['numtargets'] = 3,
		['color'] = { r=1, g=1, b=0, a=1},
		['threshold'] = 80000,
		['always_best'] = true,
		['SS'] = false,
		['IF'] = false,
		['incoming_heals'] = false,
	},
	['ch'] = {
		['enable'] = true,
		['color'] = { r=1, g=1, b=0, a=1},
		['threshold'] = 1,
		['range'] = true,
		['minjumps'] = 2,
		['maxhealth'] = 85,
	},
	['hr'] = {
		['enable'] = true,
		['numtargets'] = 4,
		['color'] = { r=1, g=1, b=0, a=1},
		['threshold'] = 1,
	},	
}

for i=10,40,15 do
local group = 'raid'..i
P.unitframe.units[group]['priestaoe'] = {}
P.unitframe.units[group].priestaoe.poh = {
	['enable'] = true,
	['size'] = 18,
	['attachTo'] = 'TOP',
	['xOffset'] = 0,
	['yOffset'] = 10,
}		
P.unitframe.units[group].priestaoe.coh = {
	['enable'] = true,
	['size'] = 18,
	['attachTo'] = 'TOP',
	['xOffset'] = 0,
	['yOffset'] = 10,
}	
P.unitframe.units[group].priestaoe.ch = {
	['enable'] = true,
	['size'] = 18,
	['attachTo'] = 'RIGHT',
	['xOffset'] = -4,
	['yOffset'] = 0,
}	
P.unitframe.units[group].priestaoe.hr = {
	['enable'] = true,
	['size'] = 18,
	['attachTo'] = 'RIGHT',
	['xOffset'] = -4,
	['yOffset'] = 0,
}	
end

				
local function print(...)
	DEFAULT_CHAT_FRAME:AddMessage("|cff33ff99EPA|r: " .. table.concat({...}, " "))
end

function EPA:Initialize()
	EPA:RegisterChatCommand("eparoster", "PrintRoster")
	EPA:RegisterEvent("PLAYER_TALENT_UPDATE", "UpdateTalents")
	EPA:RegisterEvent("PLAYER_EQUIPMENT_CHANGED", "UpdateCoefs")
	EPA:RegisterEvent("GROUP_ROSTER_UPDATE", "UpdateRoster")
	EPA:RegisterEvent("PLAYER_ENTERING_WORLD", "UpdateRoster")	
	update_frame:SetScript("OnUpdate", function(_, elapsed) return self:OnUpdate(elapsed) end)	
	
	settings = E.db.epa
	settings_coh = E.db.epa.coh
	settings_poh = E.db.epa.poh
	settings_ch  = E.db.epa.ch
	settings_hr = E.db.epa.hr
	settings_debug = E.db.epa.debug

	self:UpdateRoster()
	self:UpdateTalents()
end

function EPA:GenerateOptions()
	E.Options.args.epa = {
		order = 100,
		type = 'group',
		name = EPA.Title,
		args = {}
			
	}
	E.Options.args.epa.args.debug = {
		order = 103,
		type = 'group',
		name = 'Test Mode',
		guiInline = true,
		args = {
			one = {
				order = 1,
				type = 'toggle',
				name = 'Debug One',
				get = function () return E.db.epa.debug.one end,
				set = function (_, v) E.db.epa.debug.one = v end,	
				},			
			two = {
				order =2,
				type = 'toggle',
				name = 'Debug Two (Gain,Lost)',
				get = function () return E.db.epa.debug.two end,
				set = function (_, v) E.db.epa.debug.two = v end,	
				},			
			three = {
				order = 3,
				type = 'toggle',
				name = 'Debug Three (Visib)',
				get = function () return E.db.epa.debug.three end,
				set = function (_, v) E.db.epa.debug.three = v end,	
				},			
			four = {
				order = 4,
				type = 'toggle',
				name = 'Debug Four',
				get = function () return E.db.epa.debug.four end,
				set = function (_, v) E.db.epa.debug.four = v end,	
				},
			test = {
				order = 1,
				type = 'toggle',
				name = 'Test Mode',
				get = function () return E.db.epa.debug.testmode end,
				set = function (_, v) E.db.epa.debug.testmode = v end,	
				},
			}
	}--]]
	E.Options.args.epa.args.extras = {
		order = 101,
		type = 'group',
		name = '',
		--get = function(info) return AS:CheckOption(info[#info]) end,
		--set = function(info, value) AS:SetOption(info[#info], value) end,
		guiInline = true,
		args = {
			frequency = {
				order = 100,
				type = "range",
				name = "Refresh time",
				desc = "Seconds between status refreshes",
				width = "full",
				get = function () return E.db.epa.cycle_time end,
				set = function (_, v) E.db.epa.cycle_time = v end,
				min = 0.01,
				max = 5,
				step = 0.01,
			},
			first_groups_only = {
				order = 101,
				type = "toggle",
				name = "Check only first groups",
				desc = "Depending on the raid size, checks only the first groups to improve performance",
				get = function () return E.db.epa.first_groups_only end,
				set = function (_, v) E.db.epa.first_groups_only = v; EPA:UpdateNumGroups() end,
			},
			targets_hit = {
				order = 102,
				type = "toggle",
				name = "Show number of targets",
				desc = "Show the amount of targets you will hit with your AE heal.",
				get = function () return E.db.epa.targets_hit end,
				set = function (_, v) E.db.epa.targets_hit = v end,
			},
		}
	}
	E.Options.args.epa.args.coh = {
		type = 'group',
		name = 'CoH Options',
		order = 103,
		--get = function(info) return AS:CheckOption(info[#info]) end,
		--set = function(info, value) AS:SetOption(info[#info], value) end,
		guiInline = false,
		args = {
			coh_enable = {
						order = 1,
						type = "toggle",
						name = "Enable ",
						desc = "Enable Circle of Healing",
						get = function()
						return E.db.epa.coh.enable
						end,
						set = function(_, v)
							E.db.epa.coh.enable = v
								if not v then
									if EPA.OnStatusDisable then
										EPA:OnStatusDisable("coh")
									end
								end
						end,
			},
			coh_opacity = {
						name = "Opacity",
						desc = "Opacity for Circle of Healing",
						order = 3,
						type = "range",
						min = 0.05,
						max = 1,
						step = 0.05,
						isPercent = true,
						get = function() local color = E.db.epa.coh.color return color.a end,
						set = function(_, a) local color = E.db.epa.coh.color color.a = a or 1 end,
							--func = function() GridStatus:ResetClassColors() end,
						},			
			coh_threshold = {
						order = 4,
						type = "range",
						name = "Heal threshold",
						desc = "Threshold for heal amount",
						softMax = 250000,
						min = 1,
						step = 1,
						width = "full",
						get = function () return E.db.epa.coh.threshold end,
						set = function (_, v) E.db.epa.coh.threshold = v end,
			},
			coh_enable_if_have = {
						order = 5,
						type = "toggle",
						name = "Show only while having the talent",
						desc = "Enable this if you want to hide this status while using a talent spec that does not include Circle of Healing",
						width = "full",
						get = function () return E.db.epa.coh.enable_if_have end,
						set = function (_, v) E.db.epa.coh.enable_if_have = v; EPA:UpdateTalents(); end,
			},
			coh_hidecd = {
						order = 6,
						type = "toggle",
						name = "Hide while on cooldown",
						desc = "Hide status while Circle of Healing is on cooldown",
						width = "full",
						get = function () return E.db.epa.coh.hidecd end,
						set = function (_, arg) E.db.epa.coh.hidecd = arg end,
			}
		}
	}
	E.Options.args.epa.args.poh = {
		type = 'group',
		name = 'PoH Options',
		order = 102,
		--get = function(info) return AS:CheckOption(info[#info]) end,
		--set = function(info, value) AS:SetOption(info[#info], value) end,
		guiInline = false,
		args = {
			poh_enable = {
						order = 1,
						type = "toggle",
						name = "Enable",
						desc = "Enable Prayer of Healing",
						get = function()
						return E.db.epa.poh.enable
						end,
						set = function(_, v)
							E.db.epa.poh.enable = v
								if not v then
									if EPA.OnStatusDisable then
										EPA:OnStatusDisable("poh")
									end
								end
						end,					
			},
			poh_numtargets = {
						name = "Min Targets",
						desc = "Minimum number to show icon",
						order = 2,
						type = "range",
						min = 1,
						max = 5,
						step = 1,
						get = function() return E.db.epa.poh.numtargets end,
						set = function(_, v) E.db.epa.poh.numtargets = v end,
			},
			poh_opacity = {
						name = "Opacity",
						desc = "Opacity for Prayer of Healing",
						order = 3,
						type = "range",
						min = 0.05,
						max = 1,
						step = 0.05,
						isPercent = true,
						get = function() local color = E.db.epa.poh.color return color.a end,
						set = function(_, a) local color = E.db.epa.poh.color color.a = a or 1 end,
							--func = function() GridStatus:ResetClassColors() end,
			},			
			poh_threshold = {
						order = 4,
						type = "range",
						name = "Heal threshold",
						desc = "Threshold for heal amount",
						softMax = 250000,
						min = 1,
						step = 1,
						width = "full",
						get = function () return E.db.epa.poh.threshold end,
						set = function (_, v) E.db.epa.poh.threshold = v end,
			},
			poh_enablebest = {
						order = 5,
						type = "toggle",
						name = "Always show best target in each group",
						desc = "Shows best target to cast Prayer of Healing on in each group, even if the heal amount is below the threshold (minimum 3 targets)",
						width = "full",
						get = function () return E.db.epa.poh.always_best end,
						set = function (_, v) E.db.epa.poh.always_best = v end,
				},
			poh_SS = {
						order = 5,
						type = "toggle",
						name = "Show best if Spirit Shell if ready",
						desc = "Shows best target to cast PoH on in each group, even if the heal amount is below the threshold (minimum 3 targets) while Spirit Shell is off CD.",
						width = "full",
						get = function () return E.db.epa.poh.SS end,
						set = function (_, v) E.db.epa.poh.SS = v end,
				},
			poh_IF = {
						order = 5,
						type = "toggle",
						name = "Show best if Inner Focus if ready",
						desc = "Shows best target to cast PoH on in each group, even if the heal amount is below the threshold (minimum 3 targets) while Inner Focus is off CD.",
						width = "full",
						get = function () return E.db.epa.poh.IF end,
						set = function (_, v) E.db.epa.poh.IF = v end,
				},
			poh_incomingheals = {
						order = 6,
						type = "toggle",
						name = "Subtract incoming heals",
						desc = "Subtracts incoming heals from the health deficit before making the calculations (your own heals are excluded)",
						width = "full",
						get = function () return E.db.epa.poh.incoming_heals end,
						set = function (_, v) E.db.epa.poh.incoming_heals = v end,
			},
		}	
	}	
	E.Options.args.epa.args.ch = {
		type = 'group',
		name = 'CH Options',
		order = 104,
		--get = function(info) return AS:CheckOption(info[#info]) end,
		--set = function(info, value) AS:SetOption(info[#info], value) end,
		guiInline = false,
		args = {
			ch_enable = {
						order = 1,
						type = "toggle",
						name = "Enable",
						desc = "Enable Chain Heal",
						get = function()
						return E.db.epa.ch.enable
						end,
						set = function(_, v)
							E.db.epa.ch.enable = v
								if not v then
									if EPA.OnStatusDisable then
										EPA:OnStatusDisable("ch")
									end
								end
						end,					
			},
			ch_opacity = {
						name = "Opacity",
						desc = "Opacity for Chain Heal",
						order = 3,
						type = "range",
						min = 0.05,
						max = 1,
						step = 0.05,
						isPercent = true,
						get = function() local color = E.db.epa.ch.color return color.a end,
						set = function(_, a) local color = E.db.epa.ch.color color.a = a or 1 end,
							--func = function() GridStatus:ResetClassColors() end,
			},			
			ch_threshold = {
						order = 4,
						type = "range",
						name = "Heal threshold",
						desc = "Threshold for heal amount",
						softMax = 250000,
						min = 1,
						step = 1,
						width = "full",
						get = function () return E.db.epa.ch.threshold end,
						set = function (_, v) E.db.epa.ch.threshold = v end,
			},
			ch_minjump = {
						order = 5,
						type = "range",
						name = "Jump threshold",
						desc = "Only consider targets that will result in at least this number of Chain Heal jumps.",
						max = 3,
						min = 1,
						step = 1,
						get = function () return E.db.epa.ch.minjump end,
						set = function (_, v) E.db.epa.ch.minjump = v end,
			},
			ch_maxhealth = {
						order = 6,
						type = "range",
						name = "Health threshold",
						desc = "Only consider targets below this percentage of health.",
						max = 100,
						min = 1,
						step = 1,
						get = function () return E.db.epa.ch.maxhealth end,
						set = function (_, v) E.db.epa.ch.maxhealth = v end,
			}
		}	
	}
E.Options.args.epa.args.hr = {
		type = 'group',
		name = 'HR Options',
		order = 1,
		--get = function(info) return AS:CheckOption(info[#info]) end,
		--set = function(info, value) AS:SetOption(info[#info], value) end,
		guiInline = false,
		args = {
			hr_enable = {
						order = 1,
						type = "toggle",
						name = "Enable",
						desc = "Enable Holy Radiance",
						get = function()
						return E.db.epa.hr.enable
						end,
						set = function(_, v)
							E.db.epa.hr.enable = v
								if not v then
									if EPA.OnStatusDisable then
										EPA:OnStatusDisable("hr")
									end
								end
						end,					
			},
			hr_numtargets = {
						name = "Min Targets",
						desc = "Minimum number to show icon",
						order = 2,
						type = "range",
						min = 1,
						max = 6,
						step = 1,
						get = function() return E.db.epa.hr.numtargets end,
						set = function(_, v) E.db.epa.hr.numtargets = v end,
			},
			hr_opacity = {
						name = "Opacity",
						desc = "Opacity for Holy Radiance",
						order = 3,
						type = "range",
						min = 0.05,
						max = 1,
						step = 0.05,
						isPercent = true,
						get = function() local color = E.db.epa.hr.color return color.a end,
						set = function(_, a) local color = E.db.epa.hr.color color.a = a or 1 end,
							--func = function() GridStatus:ResetClassColors() end,
			},			
			hr_threshold = {
						order = 4,
						type = "range",
						name = "Heal threshold",
						desc = "Threshold for heal amount",
						softMax = 250000,
						min = 1,
						step = 1,
						width = "full",
						get = function () return E.db.epa.hr.threshold end,
						set = function (_, v) E.db.epa.hr.threshold = v end,
			},
		}	
	}
	
	
for i=10, 40, 15 do
	E.Options.args.unitframe.args['raid'..i].args.priestaoe = {
				order = 10000,
				type = 'group',
				name = 'ElvUI Priest AoE',
				get = function(info) return E.db.unitframe.units['raid'..i]['priestaoe'][ info[#info] ] end,
				set = function(info, value) E.db.unitframe.units['raid'..i]['priestaoe'][ info[#info] ] = value; UF:CreateAndUpdateHeaderGroup('raid'..i) end,
				args = {	
				},
	}
	
	E.Options.args.unitframe.args['raid'..i].args.priestaoe.args.poh = {
				order = 1,
				type = 'group',
				name = 'PoH Icon Options',
				get = function(info) return E.db.unitframe.units['raid'..i]['priestaoe']['poh'][ info[#info] ] end,
				set = function(info, value) E.db.unitframe.units['raid'..i]['priestaoe']['poh'][ info[#info] ] = value; UF:CreateAndUpdateHeaderGroup('raid'..i) end,
				args = {
					enable = {
						type = 'toggle',
						order = 1,
						name = 'Enable',
					},	
					attachTo = {
						type = 'select',
						order = 2,
						name = 'Position',
						values = auraAnchors,
					},
					size = {
						type = 'range',
						name = 'Size',
						order = 3,
						min = 8, max = 30, step = 1,
					},				
					xOffset = {
						order = 4,
						type = 'range',
						name = 'xOffset',
						min = -300, max = 300, step = 1,
					},
					yOffset = {
						order = 5,
						type = 'range',
						name = 'yOffset',
						min = -300, max = 300, step = 1,
					},
					test = {
						order = 6,
						type = 'toggle',
						name = 'Test Mode',
						get = function () return E.db.epa.debug.testmode end,
						set = function (_, v) E.db.epa.debug.testmode = v end,	
						},					
					},
	}
	
	E.Options.args.unitframe.args['raid'..i].args.priestaoe.args.coh = {
				order = 2,
				type = 'group',
				name = 'CoH Icon Options',
				get = function(info) return E.db.unitframe.units['raid'..i]['priestaoe']['coh'][ info[#info] ] end,
				set = function(info, value) E.db.unitframe.units['raid'..i]['priestaoe']['coh'][ info[#info] ] = value; UF:CreateAndUpdateHeaderGroup('raid'..i) end,
				args = {
					enable = {
						type = 'toggle',
						order = 1,
						name = 'Enable',
					},	
					attachTo = {
						type = 'select',
						order = 2,
						name = 'Position',
						values = auraAnchors,
					},
					size = {
						type = 'range',
						name = 'Size',
						order = 3,
						min = 8, max = 30, step = 1,
					},				
					xOffset = {
						order = 4,
						type = 'range',
						name = 'xOffset',
						min = -300, max = 300, step = 1,
					},
					yOffset = {
						order = 5,
						type = 'range',
						name = 'yOffset',
						min = -300, max = 300, step = 1,
					},	
					test = {
						order = 6,
						type = 'toggle',
						name = 'Test Mode',
						get = function () return E.db.epa.debug.testmode end,
						set = function (_, v) E.db.epa.debug.testmode = v end,	
					},		
				},
	}

	E.Options.args.unitframe.args['raid'..i].args.priestaoe.args.ch = {
				order = 2,
				type = 'group',
				name = 'CH Icon Options',
				get = function(info) return E.db.unitframe.units['raid'..i]['priestaoe']['ch'][ info[#info] ] end,
				set = function(info, value) E.db.unitframe.units['raid'..i]['priestaoe']['ch'][ info[#info] ] = value; UF:CreateAndUpdateHeaderGroup('raid'..i) end,
				args = {
					enable = {
						type = 'toggle',
						order = 1,
						name = 'Enable',
					},	
					attachTo = {
						type = 'select',
						order = 2,
						name = 'Position',
						values = auraAnchors,
					},
					size = {
						type = 'range',
						name = 'Size',
						order = 3,
						min = 8, max = 30, step = 1,
					},				
					xOffset = {
						order = 4,
						type = 'range',
						name = 'xOffset',
						min = -300, max = 300, step = 1,
					},
					yOffset = {
						order = 5,
						type = 'range',
						name = 'yOffset',
						min = -300, max = 300, step = 1,
					},	
					test = {
						order = 6,
						type = 'toggle',
						name = 'Test Mode',
						get = function () return E.db.epa.debug.testmode end,
						set = function (_, v) E.db.epa.debug.testmode = v end,	
					},		
				},
	}

	E.Options.args.unitframe.args['raid'..i].args.priestaoe.args.hr = {
				order = 2,
				type = 'group',
				name = 'HR Icon Options',
				get = function(info) return E.db.unitframe.units['raid'..i]['priestaoe']['hr'][ info[#info] ] end,
				set = function(info, value) E.db.unitframe.units['raid'..i]['priestaoe']['hr'][ info[#info] ] = value; UF:CreateAndUpdateHeaderGroup('raid'..i) end,
				args = {
					enable = {
						type = 'toggle',
						order = 1,
						name = 'Enable',
					},	
					attachTo = {
						type = 'select',
						order = 2,
						name = 'Position',
						values = auraAnchors,
					},
					size = {
						type = 'range',
						name = 'Size',
						order = 3,
						min = 8, max = 30, step = 1,
					},				
					xOffset = {
						order = 4,
						type = 'range',
						name = 'xOffset',
						min = -300, max = 300, step = 1,
					},
					yOffset = {
						order = 5,
						type = 'range',
						name = 'yOffset',
						min = -300, max = 300, step = 1,
					},	
					test = {
						order = 6,
						type = 'toggle',
						name = 'Test Mode',
						get = function () return E.db.epa.debug.testmode end,
						set = function (_, v) E.db.epa.debug.testmode = v end,	
					},		
				},
	}	
end
end

function EPA:OnStatusDisable(status)
	if num_enabled > 0 then
		if status == "coh" then
			for uid = 1, 40 do
				if UnitInRaid('raid'..uid)~=nil then
				EPA:SendStatusLost('raid'..uid, ICON_COH)
				end
			end
			--if Emphasize then Emphasize:DeemphasizeAllUnits("priestaoe_coh") end
			num_enabled = num_enabled - 1
		elseif status == "poh" then
			EPA:ClearPoHStatus(ICON_POH)
			--if Emphasize then Emphasize:DeemphasizeAllUnits("priestaoe_poh") end
			num_enabled = num_enabled - 1
		elseif status == "ch" then
			EPA:ClearCHStatus(ICON_CH)
		elseif status == "hr" then
			EPA:ClearHRStatus(ICON_HR)
		end

		if num_enabled == 0 then
			update_frame:SetScript("OnUpdate", nil)
			EPA:UnregisterEvent("PLAYER_TALENT_UPDATE")
			EPA:UnregisterEvent("PLAYER_EQUIPMENT_CHANGED")
			EPA:UnregisterEvent("GROUP_ROSTER_UPDATE")
			EPA:UnregisterMessage("Grid_PartyTransition")
			--MapFiles:UnregisterCallback("MapChanged")
		end
	end
end

local roster = {}
function EPA:UpdateRoster()
	self:UpdateNumGroups()
	wipe(roster)
	roster = { [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {} }

	if IsInRaid() then
		for i = 1, GetNumGroupMembers() do
			local name, _, group = GetRaidRosterInfo(i)
			local unitid = "raid" .. i
			if name and UnitExists(unitid) then
				tinsert(roster[group], unitid)
				--[[if UnitIsUnit("player", unitid) then  --holy nova stuff
					player_group = group
				end]]
			end
		end
	end

	--print("roster updated:", #roster[1], #roster[2], #roster[3], #roster[4], #roster[5], #roster[6], #roster[7], #roster[8])
end

function EPA:UpdateTalents()

	if (EPA.MyClass == "PRIEST") and (GetSpecialization() == TALENTTREE_HOLY) then
		-- check if player has coh glyph
		if self:HasGlyph(GLYPH_COH) then
			coh_targets = 6
		else
			coh_targets = 5
		end
	else
		coh_targets = nil
	end

	-- lost coh? clear status
	if settings_coh.enable and settings_coh.enable_if_have and not coh_targets then
		self:ClearCoHStatus()
	end

	-- update ranges
	
	if (EPA.MyClass == "SHAMAN") and self:HasGlyph(GLYPH_CH) then
		ch_range = 25
	else
		ch_range = 12.5
	end

	-- update spell coefs
	self:UpdateCoefs()
end

function EPA:UpdateNumGroups()
	raid10visibility = SecureCmdOptionParse(E.db.unitframe.units.raid10.visibility)
	raid25visibility = SecureCmdOptionParse(E.db.unitframe.units.raid25.visibility)
	raid40visibility = SecureCmdOptionParse(E.db.unitframe.units.raid40.visibility)
	
	self:ClearCoHStatus()
	self:ClearPoHStatus()
	self:ClearCHStatus()
	self:ClearHRStatus()
	if settings.first_groups_only then
		if raid10visibility == "show" then
			num_groups = raid10groups
		elseif raid25visibility == "show" then
			num_groups = raid25groups
		elseif raid40visibility == "show" then
			num_groups = raid40groups
		end	
	else
		num_groups = 8
	end
end
--[[
function EPA:GetPartyState()
		local _, instanceType = IsInInstance()

		if instanceType == "arena" then
			return "arena"
		end

		if instanceType == "pvp" or (instanceType == "none" and GetZonePVPInfo() == "combat") then
			return "bg"
		end

		if IsInRaid() then
			if instanceType == "raid" then
				local _, _, _, _, maxPlayers = GetInstanceInfo()
				return maxPlayers == 10 and "Raid10" or maxPlayers == 25 and "Raid25" or "Raid40"
			else
				return "Raid40"
			end
		end

		if IsInGroup() then
			return "party"
		end

		return "solo"
end
--]]
function EPA:ClearCoHStatus()
	if coh_prev_best.guid then
		
		EPA:SendStatusLost(coh_prev_best.uid, ICON_COH)
		--if Emphasize then Emphasize:DeemphasizeUnit(coh_prev_best.guid, "priestaoe_coh") end
		coh_prev_best.guid = nil
		coh_prev_best.uid = nil
		coh_prev_best.amount = -1
	end
end

function EPA:ClearGridStatus(icon)
	for uid = 1, 40 do
		if UnitInRaid('raid'..uid)~=nil then
			EPA:SendStatusLost('raid'..uid, icon)
		end
	end
end

function EPA:ClearTestMode(icon)
	for uid = 2, 40 do
		if UnitInRaid('raid'..uid)~=nil then
			EPA:SendStatusLost('raid'..uid, icon)
		end
	end
end	

function EPA:ClearPoHStatus()
	self:ClearGridStatus(ICON_POH)
	--poh_prev_best = nil, might not need
end

function EPA:ClearCHStatus()
	self:ClearGridStatus(ICON_CH)
end

function EPA:ClearHRStatus()
	self:ClearGridStatus(ICON_HR)
end

function EPA:PrintRoster()
	--[[self:print_r(roster)
	if raid10visibility == "show" then
		print('raid10')
	elseif raid25visibility == "show" then
		print('raid25')
	elseif raid40visibility == "show" then
		print('raid40')	
	end]]
	--self:print_r(roster)
	self:print_r(player_data['raid'..UnitInRaid("player")])
end

-- debug end

function EPA:OnUpdate(elapsed)
	update_timer = update_timer + elapsed

	-- check for coh cooldown here to update faster
	if coh_prev_best.guid and settings_coh.enable and settings_coh.hidecd and (not settings_coh.enable_if_have or coh_targets) and not self:IsCohReady() then
		self:ClearCoHStatus()
	end

	if update_timer >= settings.cycle_time then
		update_timer = 0
		EPA:RefreshAll()
	end
end

local player_data = {}

local function DistanceSq(x1, y1, x2, y2)
	local xx = x2 - x1
	local yy = y2 - y1

	return xx*xx + yy*yy
end

function EPA:IsValidTarget(unitid)
	return not UnitIsDeadOrGhost(unitid) and
				UnitIsConnected(unitid) and
				UnitIsVisible(unitid) and
				not (UnitIsCharmed(unitid) and UnitIsEnemy("player", unitid))
end

local function IsUnitInRange(unitid)
	if UnitIsUnit(unitid, "player") then
		return true
	else
		return UnitInRange(unitid)
	end
end

function EPA:RefreshMapData()


	-- check player position
	local player_x, player_y = GetPlayerMapPosition("player")

	if (player_x > 0 or player_y > 0) then
		return true
	end

	return false
end

local testmodecleared = true

function EPA:RefreshAll()
	if not self:RefreshMapData() then
		-- only happens if map is nil
		
		if settings_coh.enable then
			self:ClearCoHStatus()
		end
		
		if settings_poh.enable then
			self:ClearPoHStatus()
		end
		
		if settings_poh.enable then
			self:ClearCHStatus()
		end
			
		-- if settings_hn.enable then
			-- self:ClearHnStatus()
		-- end
		
	else
		--local ctime = GetTime()
		--local poh_time = select(7, GetSpellInfo(SPELLID_POH)) / 1000 -- Prayer of Healing cast time, in seconds

		-- cache player data
		wipe(player_data)
		
		for group = 1, num_groups do
			local unitid
			for _, unitid in pairs(roster[group]) do
				if EPA:IsValidTarget(unitid) then
					local health = UnitHealth(unitid)
					local health_max = UnitHealthMax(unitid)
					local cguid = UnitGUID(unitid)
					local percent = health / health_max


				
					player_data[unitid] = {
						guid = cguid,
						deficit = health_max - health,
						percent = percent,
						incoming = self:GetIncomingHeals(unitid),
						healmod = 1,
						inrange = {} -- list CH can jump to
					}
					
					-- make list of players in range of everyone
					local tunitid, tdata
					if EPA.MyClass == "SHAMAN" then
						--for tunitid, tdata in pairs(player_data) do
						local i
						for i = 1,25 do if UnitInRaid("raid"..i) then
							local tunitid = "raid"..i
							-- Only include targets below maxhealth %
							--print('test: ', unitid, tunitid, unitid == tunitid and 0 or E:GetDistance(unitid,tunitid))
							local dist = unitid == tunitid and 0 or E:GetDistance(unitid,tunitid) or 0
							if dist < ch_range then
								if true then --player_data[unitid].percent <= (settings_ch.maxhealth * 0.01) then
									--tinsert(tdata.inrange, tunitid)
								end
								if true then--or tdata.percent <= (settings_ch.maxhealth * 0.01) then
									tinsert(player_data[unitid].inrange, tunitid)
								end
							end
						end	end
					end

				end
			end
		end
		
		if not IsInRaid() then
			if settings_debug.testmode then
				self:ClearTestMode(ICON_POH)
				self:ClearTestMode(ICON_COH)
				self:ClearTestMode(ICON_CH)
				self:ClearTestMode(ICON_HR)
				testmodecleared = false
				self:SendStatusGained("player",ICON_POH, settings_poh.numtargets)
				self:SendStatusGained("player",ICON_COH, 5)
				self:SendStatusGained("player",ICON_CH, 4)
				self:SendStatusGained("player",ICON_HR, settings_hr.numtargets)
			elseif not settings_debug.testmode and not testmodecleared then
				testmodecleared = true
				self:SendStatusLost("player",ICON_POH)
				self:SendStatusLost("player",ICON_COH)
				self:SendStatusLost("player",ICON_CH)
				self:SendStatusLost("player",ICON_HR)
			end
		elseif IsInRaid() then
			if settings_debug.testmode then
				self:ClearTestMode(ICON_POH)
				self:ClearTestMode(ICON_COH)
				self:ClearTestMode(ICON_CH)
				self:ClearTestMode(ICON_HR)
				testmodecleared = false
				self:SendStatusGained("raid1",ICON_POH, settings_poh.numtargets)
				self:SendStatusGained("raid1",ICON_COH, 5)
				self:SendStatusGained("raid1",ICON_CH, 4)
				self:SendStatusGained("raid1",ICON_HR, settings_hr.numtargets)
			elseif not settings_debug.testmode and not testmodecleared then
				testmodecleared = true
				self:SendStatusLost("raid1",ICON_POH)
				self:SendStatusLost("raid1",ICON_COH)
				self:SendStatusLost("raid1",ICON_CH)
				self:SendStatusLost("raid1", ICON_HR)
			end
		end

		
		
		if (EPA.MyClass == "PRIEST" and GetSpecialization() == TALENTTREE_HOLY) and settings_coh.enable and
			(not settings_coh.hidecd or self:IsCohReady()) and
			(not settings_coh.enable_if_have or coh_targets) then
			self:RefreshCoH()
		end
		
		if (EPA.MyClass == "PRIEST" and GetSpecialization() ~= 3) and settings_poh.enable then
			self:RefreshPoH()
		end
		
		if EPA.MyClass == "SHAMAN" and settings_ch.enable then
			self:RefreshCH()
		end
		
		if (EPA.MyClass == "PALADIN" and GetSpecialization() == 1) and settings_hr.enable then
			self:RefreshHR()
		end

		-- -- Holy Nova
		-- if settings_hn.enable then
			-- self:RefreshHN()
		-- end
		
	end
end

-- Circle of Healing
local function CohSortTargets(x, y)
	return x.percent < y.percent
end

local targets = {}
function EPA:RefreshCoH()
	local player_data = player_data

	local coh_avg = self:GetCohAvg() * self:GetPlayerHealingMod()

	local coh_best_uid = nil
	local coh_best_amount = -1
	local coh_best_pdata = nil
	local coh_best_dists = nil
	local coh_best_targets = nil

	for unitid, p1 in pairs(player_data) do
		if IsUnitInRange(unitid) then
			wipe(targets)
			for tunitid, p2 in pairs(player_data) do
				local dist = unitid == tunitid and 0 or E:GetDistance(unitid,tunitid) or 0
				if dist <= 30 and p2.deficit > 0 then
					local amount = math_min(p2.deficit, coh_avg * p2.healmod)
					if amount > 0 then
						tinsert(targets, { amount = amount, percent = p2.percent, dist = dist })
					end
				end
			end

			-- order candidate targets, get best
			if #targets > (coh_targets or 5) then
				tsort(targets, CohSortTargets)
			end

			local dists = 0
			local hsum = 0
			for ti = 1, math_min(coh_targets or 5, #targets) do
				hsum = hsum + targets[ti].amount
				dists = math_max(dists, targets[ti].dist)
			end
			-- dists = dists / math_min(coh_targets or 5, #targets)

			-- select best, buy try to keep same target
			if hsum > coh_best_amount or
					(hsum == coh_best_amount and (
						unitid == coh_prev_best.uid or
						(coh_best_uid ~= coh_prev_best.uid and dists < coh_best_dists)
						)) then
				coh_best_uid = unitid
				coh_best_amount = hsum
				coh_best_pdata = p1
				coh_best_dists = dists
				coh_best_targets = #targets
			end
		end
	end

	-- send status to best
	if coh_best_uid ~= coh_prev_best.uid or coh_best_amount < settings_coh.threshold then
		self:ClearCoHStatus()
	end

	if coh_best_uid and coh_best_amount >= settings_coh.threshold then
	
		if coh_prev_best.uid ~= coh_best_uid or coh_prev_best.guid ~= coh_best_pdata.guid or coh_prev_best.amount ~= coh_best_amount then
			EPA:SendStatusGained(coh_best_uid, ICON_COH, coh_best_targets)
		end
		
		coh_prev_best.uid = coh_best_uid
		coh_prev_best.guid = coh_best_pdata.guid
		coh_prev_best.amount = coh_best_amount
	end
end

-- Prayer of Healing
function EPA:RefreshPoH()
	if E.db.epa.debug.four then print('RefreshPoH: ') end
	local player_data = player_data
	self:ClearPoHStatus()

	local poh_avg = self:GetPohAvg() * self:GetPlayerHealingMod()

	local best_group_guid = nil
	local best_group_amount = nil
	-- check all
	for group = 1, num_groups do
		local group_best_uid = nil
		local group_best_amount = 0
		local group_best_targets = 0
		local group_best_pdata = nil

		for _, unitid in pairs(roster[group]) do
			if UnitInRange(unitid) then
				local amount = 0
				local targets = 0

				local p1 = player_data[unitid]
				if p1 then
					for _, tunitid in pairs(roster[group]) do
						local p2 = player_data[tunitid]
						if p2 --[[ and not p2.gopoh ]] then
							local dist = unitid == tunitid and 0 or E:GetDistance(unitid,tunitid) or 0
							if dist <= 30 then
								amount = amount + math_min(math_max(0, p2.deficit - p2.incoming), poh_avg * p2.healmod)
								targets = targets + 1
							end
						end
					end

					if amount > group_best_amount or (amount == group_best_amount and targets > group_best_targets) then
						group_best_amount = amount
						group_best_uid = unitid
						group_best_targets = targets
						group_best_pdata = p1
					end
				end
			end
		end
		
		if group_best_uid and (not best_group_guid or group_best_amount > best_group_amount) then
			best_group_guid = group_best_pdata.guid
			best_group_amount = group_best_amount
		end
		local SSIF
		if settings_poh.SS or settings_poh.IF then
			local _,enabledSS,_ = GetSpellCooldown("Spirit Shell")
			local enabledSS1,_ = (select(8,UnitBuff("player", "Spirit Shell")) == 109964)
			local _,enabledIF,_ = GetSpellCooldown("Inner Focus")
			local enabledIF1,_ = UnitBuff("player", "Inner Focus")
			SSIF = (enabledSS1~= nil or enabledSS==0) or (enabledIF1 or enabledIF==0)
		end
		if group_best_uid and ( ((settings_poh.always_best or SSIF) and group_best_targets >= settings_poh.numtargets) or group_best_amount >= settings_poh.threshold) then
			local text = (group_best_amount >= settings_poh.threshold) and math_floor(group_best_amount + 0.5) or group_best_targets
			if E.db.epa.debug.one then print('Status Gained: ', group_best_uid) end
			self:SendStatusGained(group_best_uid,ICON_POH,group_best_targets)
		end
	end
	--[[
	if Emphasize and settings_poh.emphasize then
		if best_group_amount and best_group_amount > settings_poh.threshold then
			if poh_prev_best ~= best_group_guid then
				self:ClearPohEmphasizeStatus()
				poh_prev_best = best_group_guid
				Emphasize:EmphasizeUnit(best_group_guid, "priestaoe_poh", settings_poh.emphasize_priority, settings_poh.emphasize_color)
			end
		else
			self:ClearPohEmphasizeStatus()
		end
	end--]]
end

-- Chain Heal
function EPA:RefreshCH()
	local player_data = player_data
	local ch_avg = self:GetChAvg()
	local ch_best_uid = nil
	local ch_best_pdata = nil
	local ch_best_jumps = 0
	
	-- check all
	for unitid, p1 in pairs(player_data) do
		if UnitInRange(unitid, 40) then
			
			local curbest = nil
			local curbest_unitid = nil
			local curjumps = math_min(#(p1.inrange), 3)
			-- We're only interested in players that will generate at least minjumps number of jumps
			if curjumps >= settings_ch.minjumps then
			local mult = UnitBuff(unitid, "Riptide", nil, "PLAYER") ~= nil and 1.25 or 1			
			--print("mult:", UnitName(unitid), mult)
				for _, tunitid in pairs(p1.inrange) do
					local p2 = player_data[tunitid]
					local amount = mult * math_min(p2.deficit, ch_avg)
					if (not curbest) or (amount > curbest.deficit) then
						curbest = p2
						curbest_unitid = tunitid
						curbest.deficit = amount
					end
				end
	
				-- Check if we found a better target, based on number of jumps and health deficit
				if curbest and (curjumps >= ch_best_jumps) and ((not ch_best_pdata) or (curbest.deficit > ch_best_pdata.deficit)) then
					ch_best_pdata = curbest
					ch_best_uid = curbest_unitid
					ch_best_jumps = curjumps + 1
				end
			end
		end
	end
	

	self:ClearCHStatus()

	
	if ch_best_pdata then
		--[[self.core:SendStatusGained(	ch_best_pdata.guid, 
						"alert_chainhealtarget",
						settings.priority,
						nil,
						settings.color,
						tostring(ch_best_jumps),
						1, 
						nil,
						ICON_CH)--]]
		self:SendStatusGained(ch_best_uid, ICON_CH, ch_best_jumps)
		--[[if settings.showjumps then
			for _, tunitid in pairs(ch_best_pdata.inrange) do
				if tunitid ~= ch_best_uid then
					local p1 = player_data[tunitid]
					self.core:SendStatusGained(	p1.guid, 
									"alert_chainhealtarget",
									settings.priority,
									nil,
									settings.color2,
									tostring(ch_best_jumps),
									1, 
									nil,
									ICON_CH)
				end
			end
		end
	--]]
	end
end

local HRtargets = {}
function EPA:RefreshHR()
	if E.db.epa.debug.four then print('RefreshHR: ') end
	local player_data = player_data

	local hr_avg = self:GetHrAvg()

	local hr_best_uid = nil
	local hr_best_amount = -1
	local hr_best_pdata = nil
	local hr_best_dists = nil
	local hr_best_targets = ""

	for unitid, p1 in pairs(player_data) do
		if IsUnitInRange(unitid) then
			wipe(HRtargets)
			for tunitid, p2 in pairs(player_data) do
				local dist = unitid == tunitid and 0 or E:GetDistance(unitid,tunitid) or 0
				if dist <= 10 and p2.deficit > 0 then
					local amount = math_min(p2.deficit, hr_avg * 0.5)  --Holy Radiance is 50% of main target heal
					if amount > 0 then
						tinsert(HRtargets, { amount = amount, percent = p2.percent, dist = dist })
					end
				end
			end

			-- order candidate HRtargets, get best
			if #HRtargets > 6 then
				tsort(HRtargets, CohSortTargets)
			end

			local dists = 0
			local hsum = math_min(p1.deficit, hr_avg)
			for ti = 1, math_min(6, #HRtargets) do
				hsum = hsum + HRtargets[ti].amount
				dists = math_max(dists, HRtargets[ti].dist)
			end
			-- dists = dists / math_min(hr_HRtargets or 5, #HRtargets)

			-- select best, buy try to keep same target
			if hsum > hr_best_amount or
					(hsum == hr_best_amount and (
						unitid == hr_prev_best.uid or
						(hr_best_uid ~= hr_prev_best.uid and dists < hr_best_dists)
						)) then
				hr_best_uid = unitid
				hr_best_amount = hsum
				hr_best_pdata = p1
				hr_best_dists = dists
				hr_best_targets = #HRtargets
			end
		end
	end

	-- send status to best
	if (hr_best_uid ~= hr_prev_best.uid or hr_best_amount < settings_hr.threshold) and not settings_debug.testmode then
		self:ClearHRStatus()
	end

	if hr_best_uid and hr_best_amount >= settings_hr.threshold and hr_best_targets > settings_hr.numtargets then
	
		if hr_prev_best.uid ~= hr_best_uid or hr_prev_best.guid ~= hr_best_pdata.guid or hr_prev_best.amount ~= hr_best_amount then
			if E.db.epa.debug.one then print('Status Gained: ', hr_best_uid, hr_best_targets) end
			EPA:SendStatusGained(hr_best_uid, ICON_HR, hr_best_targets)
		end
		
		hr_prev_best.uid = hr_best_uid
		hr_prev_best.guid = hr_best_pdata.guid
		hr_prev_best.amount = hr_best_amount
	end
end

function EPA:SendStatusGained(uid,icon,numtargets)
local raid, raidframebutton = self:ReturnFrame(uid)
if E.db.epa.debug.two then print('Gained Return Frame: ', raid, uid) end
if not settings.targets_hit then numtargets = "" end
if raidframebutton then
	local pohorcoh
	if icon == ICON_POH then
		pohorcoh = 'poh'
	elseif icon == ICON_COH then
		pohorcoh = 'coh'
	elseif icon == ICON_CH then
		pohorcoh = 'ch'
	elseif icon == ICON_HR then
		pohorcoh = 'hr'
	end
	local anchor = E.db.unitframe.units[raid].priestaoe[pohorcoh].attachTo
	local offsetx, offsety = E.db.unitframe.units[raid].priestaoe[pohorcoh].xOffset, E.db.unitframe.units[raid].priestaoe[pohorcoh].yOffset 
	local size = E.db.unitframe.units[raid].priestaoe[pohorcoh].size
	local color = E.db.epa[pohorcoh].color

	if raidframebutton[pohorcoh] == nil then
		local aoebutton = CreateFrame("Button", "EPAbutton"..uid, raidframebutton)
		
		aoebutton:SetFrameStrata("HIGH")
		aoebutton:SetBackdropBorderColor(color.r,color.g,color.b)
		aoebutton:SetParent(raidframebutton)
		aoebutton:EnableMouse(false)
		aoebutton:Point("CENTER", raidframebutton, anchor, offsetx, offsety)
		aoebutton:SetSize(size,size);
		aoebutton:SetTemplate();
		aoebutton.icon = aoebutton:CreateTexture(nil, "ARTWORK")
		aoebutton.icon:SetTexture(icon)
		aoebutton.icon:SetTexCoord(unpack(E.TexCoords))
		aoebutton.icon:SetInside()
		aoebutton:SetAlpha(color.a)
		aoebutton.text = aoebutton:CreateFontString("OVERLAY")
		aoebutton.text:FontTemplate(nil,(size-5),"OUTLINE")
		aoebutton.text:Point("CENTER", aoebutton, "CENTER", 1,0)
		aoebutton.text:SetText(numtargets)
		raidframebutton[pohorcoh] = aoebutton
	elseif raidframebutton[pohorcoh] ~= nil then
		local aoebutton = raidframebutton[pohorcoh]
		aoebutton:ClearAllPoints()
		aoebutton:Point("CENTER", raidframebutton, anchor, offsetx, offsety)
		aoebutton:SetAlpha(color.a)	
		aoebutton.text:FontTemplate(nil,(size-5),"OUTLINE")		
		aoebutton:SetSize(size,size);
		if numtargets ~= nil then aoebutton.text:SetText(numtargets) end
	end
	raidframebutton[pohorcoh]:Show()
end
end

function EPA:CreateButton(frame,guid,icon)
end

function EPA:ReturnFrame(uid)
	if E.db.epa.debug.three then print('Visibility 10/25/40: ', raid10visibility, raid25visibility, raid40visibility) end
	if raid10visibility == "show" then
		for j = 1, raid10groups do
			for k = 1, 5 do
				if _G['ElvUF_Raid10Group'..j..'UnitButton'..k] and _G['ElvUF_Raid10Group'..j..'UnitButton'..k].unit == uid then
				return 'raid10', _G['ElvUF_Raid10Group'..j..'UnitButton'..k]
				end
			end	
		end	
	elseif raid25visibility == "show" then	
		for j = 1, raid25groups do
			for k = 1, 5 do
				if _G['ElvUF_Raid25Group'..j..'UnitButton'..k] and _G['ElvUF_Raid25Group'..j..'UnitButton'..k].unit == uid then
				return 'raid25', _G['ElvUF_Raid25Group'..j..'UnitButton'..k]
				end
			end	
		end	
	elseif raid40visibility == "show" then	
		for j = 1, raid40groups do
			for k = 1, 5 do
				if _G['ElvUF_Raid40Group'..j..'UnitButton'..k] and _G['ElvUF_Raid40Group'..j..'UnitButton'..k].unit == uid then
				return 'raid40', _G['ElvUF_Raid40Group'..j..'UnitButton'..k]
				end
			end	
		end	
	end
end

function EPA:SendStatusLost(uid, icon)
	local raid, raidframebutton = self:ReturnFrame(uid)
	if E.db.epa.debug.two then print('Lost Return Frame: ', raid, uid, raidframebutton.uid) end
	local pohorcoh 
	if icon == ICON_POH then
		pohorcoh = 'poh'
	elseif icon == ICON_COH then
		pohorcoh = 'coh'
	elseif icon == ICON_CH then
		pohorcoh = 'ch'
	elseif icon == ICON_HR then
		pohorcoh = 'hr'
	end
	if (raidframebutton and raidframebutton[pohorcoh] ~= nil) then
		raidframebutton[pohorcoh]:Hide()
		raidframebutton[pohorcoh] = nil
	end
end

-- --Holy Nova
-- local function HnSortTargets(x, y)
	-- return x.dist < y.dist
-- end

-- local inrange = {}
-- function EPA:RefreshHN()
	-- local player_data = player_data

	-- self:ClearHnStatus()

	-- if settings.first_groups_only and player_group > num_groups then
		-- return
	-- end

	-- local hn_avg = self:GetHnAvg() * self:GetPlayerHealingMod()

	-- wipe(inrange)

	-- local amount = 0

	-- for unitid, p2 in pairs(player_data) do
		-- if p2.guid == player_guid then
			-- amount = math_min(p2.deficit, hn_avg * p2.healmod)
		-- else
			-- local dist = E:GetDistance(player, unitid)
			-- if dist <= hn_testrange_sq then
				-- tinsert(inrange, { player = p2, dist = dist })
			-- end
		-- end
	-- end

	-- -- sort targets by distance and select the 4 closest to the player
	-- if #inrange > 4 then
		-- tsort(inrange, HnSortTargets)
	-- end

	-- for ti = 1, math_min(4, #inrange) do
		-- amount = amount + math_min(inrange[ti].player.deficit, hn_avg * inrange[ti].player.healmod)
	-- end

	-- if amount >= settings_hn.threshold then
		-- self.core:SendStatusGained(	player_guid,
									-- "alert_priestaoe_hn",
									-- settings_hn.priority,
									-- nil,
									-- settings_hn.color,
									-- tostring(math_floor(amount + 0.5)),
									-- 1,
									-- nil,
									-- ICON_HN)
		-- if settings_hn.emphasize and Emphasize then
			-- Emphasize:EmphasizeUnit(player_guid, "priestaoe_hn", settings_hn.color)
		-- end
	-- end
-- end

function EPA:HasGlyph(gid)
	for i = 2,6,2 do
		local _, _, _, id = GetGlyphSocketInfo(i)
		if id and gid == id then
			return true
		end
	end
	return false
end

function EPA:GetSetAmount(set)
	local amount = 0
	for _, itemID in ipairs(set) do
		if IsEquippedItem(itemID) then
			amount = amount + 1
		end
	end
	return amount
end

function EPA:IsCohReady()
	local start, duration = GetSpellCooldown(SPELLID_COH)

	-- ignore global cooldown, show if ready or less than 0.5 secs of cooldown remaining
	if start == 0 or duration <= 1.5 or ((duration - (GetTime() - start)) <= 0.5) then
		return true
	end

	return false
end

function EPA:UnitHasPlayerAura(unitid, aura)
	local name, _, _, _, _, _, expires, caster = UnitBuff(unitid, aura)
	return name and caster == "player" and expires
end

-- TODO: probably would be best to create a library to calculate this
function EPA:GetPlayerHealingMod()
	local sanctuary = UnitBuff("player", "Chakra: Sanctuary") and 1 or 0
	local mod = 0.25 * sanctuary
	return 1 + mod
end

function EPA:GetHealModifier(guid)
	return 1
end

function EPA:GetIncomingHeals(unitid)
	if settings_poh.enable and settings_poh.incoming_heals then
		return (UnitGetIncomingHeals(unitid) or 0) - (UnitGetIncomingHeals(unitid, "player") or 0)
	end
	return 0
end

function EPA:UpdateCoefs()
	local archangel = 1 + (((select(15, UnitBuff("player","Archangel")) or 0)/100));
	local tof = (UnitBuff("player","Twist of Fate") and 1.15) or 1 

	-- Prayer of Healing
	poh_coef_m = archangel * tof
	poh_coef_madd = 0

	test_of_faith_mul = 1
	coh_coef_madd = 0
	coh_coef_m = 1

	
	hn_coef_madd = 0
	hn_coef_m = 1
end

function EPA:GetSpellAvg(base, coef_madd, coef_m, spellmod)
	local coef = (1 + coef_madd) * coef_m
	local spellcrit = GetSpellCritChance(2)
	local spellbonus = GetSpellBonusHealing()

	local total = (base + spellbonus * spellmod) * coef
	-- total = total * (1 + spellcrit / 200.0) -- apply crit

	return math_floor(total + 0.5)
end

function EPA:GetCohAvg()
	return self:GetSpellAvg(COH_BASE, coh_coef_madd, coh_coef_m, SPELLMOD_COH)
end

function EPA:GetPohAvg()
	return self:GetSpellAvg(POH_BASE, poh_coef_madd, poh_coef_m, SPELLMOD_POH)
end

function EPA:GetHnAvg()
	return self:GetSpellAvg(HN_BASE, hn_coef_madd, hn_coef_m, SPELLMOD_HN)
end

function EPA:GetChAvg()
	return self:GetSpellAvg(CH_BASE, 0, 1, SPELLMOD_CH)
end

function EPA:GetHrAvg()
	return self:GetSpellAvg(HR_Base, .05, 1, SPELLMOD_HR)
end

function EPA:print_r ( t )
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    else
                        print(indent.."["..pos.."] =>* "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    sub_print_r(t," ")
end 

EP:RegisterPlugin(..., EPA.GenerateOptions)
E:RegisterModule(EPA:GetName())