﻿## Interface: 50400
## Title: |cff1784d1ElvUI |r|cffFFFFFFPriestAoe|r
## Notes: Shows optimal target to use with priest area healing spells
## RequiredDeps: ElvUI
## Author: Xellis (InKahootz on tukui.org)
## Version: v1.03
## SavedVariables: refresh_state
## X-Curse-Packaged-Version: 1.3
## X-Curse-Project-Name: ElvUI_PriestAoe
## X-Curse-Project-ID: ElvUI_PriestAoe
## X-Curse-Repository-ID: wow/elvui_priestaoe/mainline


ElvUI_PriestAoe.lua
libs\load_libs.xml

## X-Tukui-ProjectID: 208
## X-Tukui-ProjectFolders: ElvUI_PriestAoe